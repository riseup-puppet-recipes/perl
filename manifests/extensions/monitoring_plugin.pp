class perl::extensions::monitoring_plugin {

  case $::operatingsystem {
    'debian','ubuntu': {
      case $::operatingsystemmajrelease {
        '7': {
          # wheezy-backports-sloppy
          $version = '0.39-1~bpo7+1'
        }
        '8': {
          # jessie-backports
          $version = '0.39-1~bpo8+1'
        }
        default: {
          $version = installed
        }
      }
    }
    default: {
      $version = installed
    }
  }

  perl::module { 'monitoring-plugin': ensure => $version }

}
